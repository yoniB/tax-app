import "./App.css";
import AddTax from "./component/AddTax/AddTax";

function App() {
  return (
    <div className="App">
      <AddTax />
    </div>
  );
}

export default App;
