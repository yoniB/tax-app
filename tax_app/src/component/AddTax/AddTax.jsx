import React, { useState, useRef } from "react";
import { Formik, Field, Form } from "formik";
import "./AddTax.css";

import SeachTaxItem from "./SeachTaxItem";
import ApplicableItemSelecter from "./ApplicableItemSelecter";
import Resource from "../../master-data/Resource.json";

const AddTax = () => {
  const [selectTaxItems, setSelectTaxItems] = useState(0);
  const [selectAllBracelet, setSelectAllBracelet] = useState(false);
  const [selectAllOtherTaxItem, setSelectAllOtherTaxItem] = useState(false);
  const [allTaxItemSelected, setAllTaxItemSelected] = useState(false);
  const [braceletsList, setBraceletsList] = useState(
    Resource.filter((x) => x.category?.name === "Bracelets")
  );
  const [otherList, setOtherList] = useState(
    Resource.filter((x) => x.category === undefined)
  );
  const formikRef = useRef(null);

  const applicableItemsHandler = (event) => {
    if (event.target.type === "checkbox") {
      if (event.target.checked) {
        if (selectTaxItems === 7) {
          setAllTaxItemSelected(true);
        }
        setSelectTaxItems(selectTaxItems + 1);
      } else {
        setSelectTaxItems(selectTaxItems - 1);
        setAllTaxItemSelected(false);
        if (event.target.attributes.applicabletype.nodeValue === "other") {
          setSelectAllOtherTaxItem(false);
        } else {
          setSelectAllBracelet(false);
        }
      }
    }
  };

  const onSearch = (event) => {
    formikRef.current.values.applicable_items = [];
    setSelectTaxItems(0);
    setAllTaxItemSelected(false);
    setSelectAllOtherTaxItem(false);
    setSelectAllBracelet(false);
    if (event.target.value === "") {
      setBraceletsList(
        Resource.filter((x) => x.category?.name === "Bracelets")
      );
      setOtherList(Resource.filter((x) => x.category === undefined));
    } else {
      var tempBraceletsList = [
        ...Resource.filter((x) => x.category?.name === "Bracelets"),
      ];
      var tempOtherList = [Resource.filter((x) => x.category === undefined)];
      setBraceletsList(
        tempBraceletsList.filter(
          (x) =>
            x.name.toLowerCase().includes(event.target.value.toLowerCase()) ===
            true
        )
      );
      setOtherList(
        tempOtherList.filter(
          (x) =>
            x.name.toLowerCase().includes(event.target.value.toLowerCase()) ===
            true
        )
      );
    }
  };

  const selectAllTaxItemHandler = () => {
    formikRef.current.values.applicable_items = [];
    Resource.forEach((rosouce) => {
      formikRef.current.values.applicable_items.push(rosouce.id.toString());
    });

    setAllTaxItemSelected(true);
    setSelectAllBracelet(true);
    setSelectAllOtherTaxItem(true);
    setSelectTaxItems(8);
  };

  const selectAllBraceletsHandler = (event) => {
    var tempTaxItemList = [...formikRef.current.values.applicable_items];
    if (event.target.checked) {
      braceletsList.forEach((bracelets) => {
        tempTaxItemList.push(bracelets.id.toString());
      });
      formikRef.current.values.applicable_items = tempTaxItemList.filter(
        (v, i, a) => a.indexOf(v) === i
      );
      setSelectAllBracelet(true);
    } else {
      braceletsList.forEach((bracelets) => {
        tempTaxItemList = removeItemFromTaxList(
          tempTaxItemList,
          bracelets.id.toString()
        );
      });
      formikRef.current.values.applicable_items = tempTaxItemList;
      setSelectAllBracelet(false);
    }
  };

  const selectAllOtherTaxItemHandler = (event) => {
    var tempTaxItemList = [...formikRef.current.values.applicable_items];
    if (event.target.checked) {
      otherList.forEach((otherTax) => {
        tempTaxItemList.push(otherTax.id.toString());
      });
      formikRef.current.values.applicable_items = tempTaxItemList.filter(
        (v, i, a) => a.indexOf(v) === i
      );
      setSelectAllOtherTaxItem(true);
    } else {
      otherList.forEach((bracelets) => {
        tempTaxItemList = removeItemFromTaxList(
          tempTaxItemList,
          bracelets.id.toString()
        );
      });
      formikRef.current.values.applicable_items = tempTaxItemList;
      setSelectAllOtherTaxItem(false);
    }
  };

  const removeItemFromTaxList = (arr, value) => {
    var index = arr.indexOf(value);
    if (index > -1) {
      arr.splice(index, 1);
    }
    return arr;
  };

  const nameInputValidation = (value) => {
    if (value === "" || value === null) {
      return "error";
    }
  };

  const rateInputValidation = (value) => {
    if (value === "" || value === null) {
      return "error";
    }
  };

  return (
    <div className="add-tax-container">
      <Formik
        innerRef={formikRef}
        initialValues={{
          applicable_items: [],
          applied_to: "some",
          name: "",
          rate: 5,
        }}
        onSubmit={async (values) => {
          var displyValue = Object.assign({}, values);
          displyValue.rate = displyValue.rate / 100;
          alert(JSON.stringify(displyValue, null, 2));
        }}
        className="add-tax-container"
      >
        {({ errors, touched }) => (
          <Form onChange={applicableItemsHandler} className="add-tax-container">
            <div className="add-tax-form-container">
              <h1 className="add-tax-h1 add-tax-gray">Add Tax</h1>
              <div className="add-tax-top-form">
                <Field
                  id="name"
                  name="name"
                  placeholder="Tax Name"
                  type="text"
                  className={
                    errors.name === "error"
                      ? touched.name
                        ? "add-tax-input add-tax-input-invalid"
                        : "add-tax-input"
                      : "add-tax-input"
                  }
                  validate={nameInputValidation}
                />

                <span>
                  <Field
                    id="rate"
                    name="rate"
                    type="number"
                    placeholder="Tax Value"
                    className={
                      errors.rate === "error"
                        ? touched.rate
                          ? "add-tax-input add-tax-no-input add-tax-input-invalid"
                          : "add-tax-input add-tax-no-input"
                        : "add-tax-input add-tax-no-input"
                    }
                    validate={rateInputValidation}
                  />
                  <span className="add-tax-dollar-icon">%</span>
                </span>
              </div>
              <ApplicableItemSelecter
                allTaxItemSelected={allTaxItemSelected}
                selectAllTaxItemHandler={selectAllTaxItemHandler}
                setAllTaxItemSelected={setAllTaxItemSelected}
              />
              <div className="search-container">
                <SeachTaxItem onSearch={onSearch} />
                {braceletsList.length !== 0 && (
                  <div className="bracelets-container w-100">
                    <div className="search-header margin-top">
                      <label
                        className="add-tax-checkbox add-text-sm w-100 h-100 d-flex add-tax-checkbox-checkbox"
                        htmlFor="bracelets"
                      >
                        Bracelets
                        <input
                          type="checkbox"
                          value="braceletsAll"
                          id="bracelets"
                          checked={selectAllBracelet}
                          onChange={(event) => selectAllBraceletsHandler(event)}
                        />
                        <span className="add-tax-checkmark-checkbox  "></span>
                      </label>
                    </div>
                    <div className="search-checkbox-list margin-top">
                      {braceletsList.map((bracelets, index) => (
                        <label
                          className="add-tax-checkbox add-text-sm add-tax-checkbox-checkbox"
                          htmlFor={"bracelets-" + bracelets.id}
                          key={index}
                        >
                          {bracelets.name}
                          <Field
                            type="checkbox"
                            name="applicable_items"
                            value={bracelets.id.toString()}
                            id={"bracelets-" + bracelets.id}
                            applicabletype="bracelets"
                          />
                          <span className="add-tax-checkmark-checkbox"></span>
                        </label>
                      ))}
                    </div>
                  </div>
                )}

                {otherList.length !== 0 && (
                  <div className="other-container w-100">
                    <div className="search-header margin-top">
                      <label
                        className="add-tax-checkbox add-text-sm  w-100 h-100 add-tax-checkbox-checkbox"
                        htmlFor="othertax"
                      >
                        <input
                          type="checkbox"
                          name="othertax"
                          value="otherTaxAll"
                          id="othertax"
                          checked={selectAllOtherTaxItem}
                          onChange={(event) =>
                            selectAllOtherTaxItemHandler(event)
                          }
                        />
                        <span className="add-tax-checkmark-checkbox  "></span>
                      </label>
                    </div>
                    <div className="search-checkbox-list margin-top">
                      {otherList.map((other, index) => (
                        <label
                          className="add-tax-checkbox add-text-sm add-tax-checkbox-checkbox"
                          htmlFor={"other-" + other.id}
                          key={index}
                        >
                          {other.name}
                          <Field
                            type="checkbox"
                            name="applicable_items"
                            value={other.id.toString()}
                            id={"other-" + other.id}
                            applicabletype="other"
                          />
                          <span className="add-tax-checkmark-checkbox"></span>
                        </label>
                      ))}
                    </div>
                  </div>
                )}
                {(braceletsList.length !== 0 || otherList.length !== 0) && (
                  <div className="submit-btn-container">
                    <button className="submit-btn" type="submit">
                      Apply tax to {selectTaxItems} item(s)
                    </button>
                  </div>
                )}

                <div>
                  {braceletsList.length === 0 && otherList.length === 0 && (
                    <h1 className="add-tax-gray add-tax-h1 add-tax-text-center ">
                      No Items Found
                    </h1>
                  )}
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
export default AddTax;
