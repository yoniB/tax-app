const ApplicableItemSelecter = (props) => {
  return (
    <div className="add-tax-form-check-box">
      <label
        className="add-tax-checkbox add-text-sm add-tax-checkbox-radio"
        htmlFor="all-items"
      >
        Apply to all items in collection
        <input
          type="radio"
          name="selected-items"
          value="all"
          id="all-items"
          checked={props.allTaxItemSelected}
          onChange={props.selectAllTaxItemHandler}
        />
        <span className="add-tax-checkmark-radio"></span>
      </label>

      <label
        className="add-tax-checkbox add-text-sm add-tax-checkbox-radio"
        htmlFor="specific-items"
      >
        Apply to specific items
        <input
          type="radio"
          name="selected-items"
          value="specific"
          id="specific-items"
          checked={!props.allTaxItemSelected}
          onChange={() => {
            props.setAllTaxItemSelected(false);
          }}
        />
        <span className="add-tax-checkmark-radio "></span>
      </label>
    </div>
  );
};
export default ApplicableItemSelecter;
