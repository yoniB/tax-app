const SeachTaxItem = (props) => {
  return (
    <div className="d-flex">
      <i className="bi bi-search"></i>
      <input
        id="name"
        name="searchItem"
        placeholder="Search Items"
        type="text"
        className="add-tax-input add-tax-input-search-input"
        onChange={props.onSearch}
      />
    </div>
  );
};
export default SeachTaxItem;
